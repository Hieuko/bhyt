package net.springboot.javaguides.entity;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestDoanhNghiepCalculation  {
	@Test
	public void TestEmptyList() {
		List<NhanVien> list=new ArrayList<>();
		int total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(0, total);
	}
	@Test
	public void TestEmptyNhanvien() {
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien());
		int total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(0, total);
	}
	
	@Test
	public void Test_mucLuong_phuCap_bang_0() {
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(0L,0L));
		int total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(0, total);
	}
	@Test
	public void Test_mucLuong_nho_hon_0(){
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(-1L,1000000L));
		int total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(45000, total);
	}
	@Test
	public void Test_phuCap_nho_hon_0(){
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(1000000L,-1L));
		int total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(45000, total);
	}
	@Test
	public void Test_mucLuong_bang_0() {
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(0L,10000000L));
		int total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(450000, total);
	}
	@Test
	public void Test_phuCap_bang_0() {
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(10000000L,0L));
		int total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(450000, total);
	}
	@Test
	public void Test_mucLuong_phuCap_Lon_Hon_0() {
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(1000000L,1000000L));
		int total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(90000, total);
	}
	@Test
	public void Test_2_NhanVien() {
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(1000000L,1000000L));
		list.add(new NhanVien(1000000L,1000000L));
		int total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(180000, total);
	}
}
