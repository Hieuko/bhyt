package net.springboot.javaguides;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import net.springboot.javaguides.controller.UserRegistrationController;
import net.springboot.javaguides.repository.UserRepository;
import net.springboot.javaguides.service.UserService;

@SpringBootTest
public class SpringbootThymeleafWebAppApplicationTests {
	@Autowired
	private ApplicationContext context;
	
	@Test
	public void contextLoads(){
		Assertions.assertTrue(this.context.getBean(UserService.class) != null);
		Assertions.assertTrue(this.context.getBean(UserRepository.class) != null);
		Assertions.assertTrue(this.context.getBean(UserRegistrationController.class) != null);
	}

}
