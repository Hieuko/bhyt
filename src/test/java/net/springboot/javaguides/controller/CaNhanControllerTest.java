package net.springboot.javaguides.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import net.springboot.javaguides.controller.dto.UserRegistrationDto;
import net.springboot.javaguides.entity.CaNhan;
import net.springboot.javaguides.entity.User;
import net.springboot.javaguides.repository.CaNhanRepository;
import net.springboot.javaguides.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
class CaNhanControllerTest {
	@Autowired
	private ApplicationContext context;
	@Autowired
	private CaNhanController caNhanController;
	
	@Test
	void testCNform_CaNhanController_DaKhaiTT() {
		BCryptPasswordEncoder passwordEncoder = this.context.getBean(BCryptPasswordEncoder.class);
		CaNhanRepository caNhanRepository = this.context.getBean(CaNhanRepository.class);
		UserService userService = this.context.getBean(UserService.class);
		Authentication auth = new Authentication() {
			
			public Object getPrincipal() {
				UserDetails user = userService.loadUserByUsername("nguyena");
				return user;
			}
			
			@Override
			public Object getDetails() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Object getCredentials() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAuthenticated() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
				// TODO Auto-generated method stub
				
			}
		};
		SecurityContextHolder.getContext().setAuthentication(auth);
		CaNhan caNhan = new CaNhan();
		Model model = new Model() {
			
			@Override
			public Model mergeAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean containsAttribute(String attributeName) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public Map<String, Object> asMap() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(String attributeName, Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Collection<?> attributeValues) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		String s = caNhanController.CNform(caNhan, model);
		assertEquals("in4-canhan", s);
	}
	
	@Test
	void testCNform_CaNhanController_ChuaKhaiTT() {
		BCryptPasswordEncoder passwordEncoder = this.context.getBean(BCryptPasswordEncoder.class);
		CaNhanRepository caNhanRepository = this.context.getBean(CaNhanRepository.class);
		UserService userService = this.context.getBean(UserService.class);
		
		UserRegistrationDto user = new UserRegistrationDto();
		user.setFirstName("Hieu");
		user.setLastName("Nguyen");
		user.setEmail("test");
		user.setPassword("123456");
		userService.save(user);
		
		Authentication auth = new Authentication() {
			
			public Object getPrincipal() {
				UserDetails user = userService.loadUserByUsername("test");
				return user;
			}
			
			@Override
			public Object getDetails() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Object getCredentials() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAuthenticated() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
				// TODO Auto-generated method stub
				
			}
		};
		SecurityContextHolder.getContext().setAuthentication(auth);
		CaNhan caNhan = new CaNhan();
		Model model = new Model() {
			
			@Override
			public Model mergeAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean containsAttribute(String attributeName) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public Map<String, Object> asMap() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(String attributeName, Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Collection<?> attributeValues) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		String s = caNhanController.CNform(caNhan, model);
		assertEquals("dki-canhan-form", s);
	}
	
	@Test
	void testaAddCaNhan_CaNhanController() {
		BCryptPasswordEncoder passwordEncoder = this.context.getBean(BCryptPasswordEncoder.class);
		CaNhanRepository caNhanRepository = this.context.getBean(CaNhanRepository.class);
		UserService userService = this.context.getBean(UserService.class);
		
		UserRegistrationDto user = new UserRegistrationDto();
		user.setFirstName("Hieu");
		user.setLastName("Nguyen");
		user.setEmail("test");
		user.setPassword("123456");
		userService.save(user);
		
		Authentication auth = new Authentication() {
			
			public Object getPrincipal() {
				UserDetails user = userService.loadUserByUsername("test");
				return user;
			}
			
			@Override
			public Object getDetails() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Object getCredentials() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAuthenticated() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
				// TODO Auto-generated method stub
				
			}
		};
		SecurityContextHolder.getContext().setAuthentication(auth);
		
		CaNhan c = new CaNhan();
		c.setName("test1");
		c.setNgaySinh(new Date());
		c.setGioiTinh("Nam");
		c.setSoCMND("12345678901");
		c.setDiaChi("Ha Noi");
		c.setEmail("test@gmail.com");
		c.setSoDT("0123456789");
		c.setThanhPho("Ha Noi");
		c.setquan("Ha Noi");
		
		String s = caNhanController.addCaNhan(c);
		assertEquals("redirect:/canhan/form?success", s);
		
		User u = userService.getUser("test");
		CaNhan caNhan = caNhanRepository.findbyUserId(u.getId());
		assertEquals(c, caNhan);
	}
}
