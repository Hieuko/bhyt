package net.springboot.javaguides.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

//@RunWith(SpringRunner.class)
//@WebMvcTest(MainController.class)
//@ContextConfiguration(classes = { MainController.class })
@SpringBootTest
public class MainControllerTest {

	@Autowired
//	private MockMvc mvc;
	private ApplicationContext context;
	
	@Test
	public void testLogin_MainController_Successs(){
		MainController mainController = context.getBean(MainController.class);
		assertEquals("login", mainController.login());
	}
	
	@Test
	public void testHome_MainController_Successs(){
		MainController mainController = context.getBean(MainController.class);
		assertEquals("index", mainController.home());
	}
	
	@Test
	public void testSelectType_MainController_Successs(){
		MainController mainController = context.getBean(MainController.class);
		assertEquals("select-bhyt-type", mainController.selectType());
	}

}
