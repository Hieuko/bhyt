package net.springboot.javaguides.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import net.springboot.javaguides.controller.dto.UserRegistrationDto;
import net.springboot.javaguides.entity.DoanhNghiep;
import net.springboot.javaguides.entity.User;
import net.springboot.javaguides.repository.DoanhNghiepRepository;
import net.springboot.javaguides.repository.NhanVienRepository;
import net.springboot.javaguides.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
class DoanhNghiepControllerTest {
	@Autowired
	private ApplicationContext context;
	@Autowired
	private DoanhNghiepController doanhNghiepController;
	
	@Test
	void testShowForm_DoanhNghiepController_ChuaDangKyDoanhNghiep() {
		BCryptPasswordEncoder passwordEncoder = this.context.getBean(BCryptPasswordEncoder.class);
		UserService userService = this.context.getBean(UserService.class);
		DoanhNghiepRepository doanhNghiepRepository = this.context.getBean(DoanhNghiepRepository.class);
		NhanVienRepository nhanVienRepository = this.context.getBean(NhanVienRepository.class);
		UserRegistrationDto user = new UserRegistrationDto();
		user.setFirstName("Hieu");
		user.setLastName("Nguyen");
		user.setEmail("test");
		user.setPassword("123456");
		userService.save(user);
		
		Authentication auth = new Authentication() {

			public Object getPrincipal() {
				UserDetails user = userService.loadUserByUsername("test");
				return user;
			}

			@Override
			public Object getDetails() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getCredentials() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAuthenticated() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
				// TODO Auto-generated method stub

			}
		};
		SecurityContextHolder.getContext().setAuthentication(auth);
		Model model = new Model() {
			
			@Override
			public Model mergeAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean containsAttribute(String attributeName) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public Map<String, Object> asMap() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(String attributeName, Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Collection<?> attributeValues) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		DoanhNghiep doanhNghiep = new DoanhNghiep();
		String s = doanhNghiepController.showForm(doanhNghiep, model);
		assertEquals("dki-doanhNghiep-form", s);
	}
	
	@Test
	void testShowForm_DoanhNghiepController_DaDangKyDoanhNghiep() {
		BCryptPasswordEncoder passwordEncoder = this.context.getBean(BCryptPasswordEncoder.class);
		UserService userService = this.context.getBean(UserService.class);
		DoanhNghiepRepository doanhNghiepRepository = this.context.getBean(DoanhNghiepRepository.class);
		NhanVienRepository nhanVienRepository = this.context.getBean(NhanVienRepository.class);
		UserRegistrationDto user = new UserRegistrationDto();
		user.setFirstName("Hieu");
		user.setLastName("Nguyen");
		user.setEmail("test");
		user.setPassword("123456");
		userService.save(user);
		
		Authentication auth = new Authentication() {

			public Object getPrincipal() {
				UserDetails user = userService.loadUserByUsername("test");
				return user;
			}

			@Override
			public Object getDetails() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getCredentials() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAuthenticated() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
				// TODO Auto-generated method stub

			}
		};
		SecurityContextHolder.getContext().setAuthentication(auth);
		Model model = new Model() {
			
			@Override
			public Model mergeAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean containsAttribute(String attributeName) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public Map<String, Object> asMap() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(String attributeName, Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Collection<?> attributeValues) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		DoanhNghiep doanhNghiep = new DoanhNghiep();
		User u = userService.getUser("test");
		doanhNghiep.setUserDN(u);
        doanhNghiepRepository.save(doanhNghiep);
		String s = doanhNghiepController.showForm(doanhNghiep, model);
		assertEquals("list-nhan-vien", s);
	}
	
	@Test
	void testNewDoanhNghiep_DoanhNghiepController() {
		BCryptPasswordEncoder passwordEncoder = this.context.getBean(BCryptPasswordEncoder.class);
		UserService userService = this.context.getBean(UserService.class);
		DoanhNghiepRepository doanhNghiepRepository = this.context.getBean(DoanhNghiepRepository.class);
		NhanVienRepository nhanVienRepository = this.context.getBean(NhanVienRepository.class);
		UserRegistrationDto user = new UserRegistrationDto();
		user.setFirstName("Hieu");
		user.setLastName("Nguyen");
		user.setEmail("test");
		user.setPassword("123456");
		userService.save(user);
		
		Authentication auth = new Authentication() {

			public Object getPrincipal() {
				UserDetails user = userService.loadUserByUsername("test");
				return user;
			}

			@Override
			public Object getDetails() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getCredentials() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAuthenticated() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
				// TODO Auto-generated method stub

			}
		};
		SecurityContextHolder.getContext().setAuthentication(auth);
		Model model = new Model() {
			
			@Override
			public Model mergeAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean containsAttribute(String attributeName) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public Map<String, Object> asMap() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(String attributeName, Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Collection<?> attributeValues) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		DoanhNghiep d = new DoanhNghiep();
		d.setTen("TestABC");
		d.setMaDonVi("9999");
		d.setMaThue("123456");
		d.setDiaChiKinhDoanh("Ha Noi");
		d.setLoaiHinhKinhDoanh("Công ty TNHH một thành viên");
		d.setDiaChiLienHe("Ha Noi");
		d.setSoDT("0123456789");
		d.setNoiCap("Ha Noi");
		d.setPhuongThucDong("03 Thang");
		String s = doanhNghiepController.newDoanhNghiep(d, model);
		User u = userService.getUser("test");
		DoanhNghiep dv = doanhNghiepRepository.findbyUserId(u.getId());
		assertEquals(d, dv);
		assertEquals("list-nhan-vien", s);
	}

}
