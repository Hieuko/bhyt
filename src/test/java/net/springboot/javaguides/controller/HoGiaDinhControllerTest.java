package net.springboot.javaguides.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import net.springboot.javaguides.controller.dto.UserRegistrationDto;
import net.springboot.javaguides.entity.CaNhan;
import net.springboot.javaguides.entity.HoGiaDinh;
import net.springboot.javaguides.entity.User;
import net.springboot.javaguides.repository.CaNhanRepository;
import net.springboot.javaguides.repository.HoGiaDinhRepository;
import net.springboot.javaguides.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
class HoGiaDinhControllerTest {
	@Autowired
	private ApplicationContext context;
	@Autowired
	private HoGiaDinhController hoGiaDinhController;

	@Test
	void testDSDangKi_HoGiaDinhController_ChuaKhaiTT() {
		BCryptPasswordEncoder passwordEncoder = this.context.getBean(BCryptPasswordEncoder.class);
		CaNhanRepository caNhanRepository = this.context.getBean(CaNhanRepository.class);
		UserService userService = this.context.getBean(UserService.class);
		
		UserRegistrationDto user = new UserRegistrationDto();
		user.setFirstName("Hieu");
		user.setLastName("Nguyen");
		user.setEmail("test");
		user.setPassword("123456");
		userService.save(user);
		
		Authentication auth = new Authentication() {

			public Object getPrincipal() {
				UserDetails user = userService.loadUserByUsername("test");
				return user;
			}

			@Override
			public Object getDetails() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getCredentials() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAuthenticated() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
				// TODO Auto-generated method stub

			}
		};
		SecurityContextHolder.getContext().setAuthentication(auth);
		Model model = new Model() {

			@Override
			public Model mergeAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean containsAttribute(String attributeName) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Map<String, Object> asMap() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Model addAttribute(String attributeName, Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Model addAttribute(Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Model addAllAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Model addAllAttributes(Collection<?> attributeValues) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		String s = hoGiaDinhController.dsDangKi(model);
		assertEquals("redirect:/canhan/form", s);
	}
	
	@Test
	void testDSDangKi_HoGiaDinhController_DaKhaiTT_ChuaCoHGD() {
		BCryptPasswordEncoder passwordEncoder = this.context.getBean(BCryptPasswordEncoder.class);
		CaNhanRepository caNhanRepository = this.context.getBean(CaNhanRepository.class);
		HoGiaDinhRepository hoGiaDinhRepository = this.context.getBean(HoGiaDinhRepository.class);
		UserService userService = this.context.getBean(UserService.class);
		CaNhanController caNhanController = this.context.getBean(CaNhanController.class);
		
		UserRegistrationDto user = new UserRegistrationDto();
		user.setFirstName("Hieu");
		user.setLastName("Nguyen");
		user.setEmail("test");
		user.setPassword("123456");
		userService.save(user);
		
		Authentication auth = new Authentication() {

			public Object getPrincipal() {
				UserDetails user = userService.loadUserByUsername("test");
				return user;
			}

			@Override
			public Object getDetails() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getCredentials() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAuthenticated() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
				// TODO Auto-generated method stub

			}
		};
		SecurityContextHolder.getContext().setAuthentication(auth);
		
		CaNhan c1 = new CaNhan();
		c1.setName("test1");
		c1.setNgaySinh(new Date());
		c1.setGioiTinh("Nam");
		c1.setSoCMND("12345678901");
		c1.setDiaChi("Ha Noi");
		c1.setEmail("test@gmail.com");
		c1.setSoDT("0123456789");
		c1.setThanhPho("Ha Noi");
		c1.setquan("Ha Noi");
		caNhanController.addCaNhan(c1);
		
		Model model = new Model() {

			@Override
			public Model mergeAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean containsAttribute(String attributeName) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Map<String, Object> asMap() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Model addAttribute(String attributeName, Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Model addAttribute(Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Model addAllAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Model addAllAttributes(Collection<?> attributeValues) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		String s = hoGiaDinhController.dsDangKi(model);
		assertEquals("/list", s);
	}
	
	@Test
	void testDSDangKi_HoGiaDinhController_DaKhaiTT_CoHGD() {
		BCryptPasswordEncoder passwordEncoder = this.context.getBean(BCryptPasswordEncoder.class);
		CaNhanRepository caNhanRepository = this.context.getBean(CaNhanRepository.class);
		HoGiaDinhRepository hoGiaDinhRepository = this.context.getBean(HoGiaDinhRepository.class);
		UserService userService = this.context.getBean(UserService.class);
		CaNhanController caNhanController = this.context.getBean(CaNhanController.class);
		
		UserRegistrationDto user = new UserRegistrationDto();
		user.setFirstName("Hieu");
		user.setLastName("Nguyen");
		user.setEmail("test");
		user.setPassword("123456");
		userService.save(user);
		
		Authentication auth = new Authentication() {

			public Object getPrincipal() {
				UserDetails user = userService.loadUserByUsername("test");
				return user;
			}

			@Override
			public Object getDetails() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getCredentials() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAuthenticated() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
				// TODO Auto-generated method stub

			}
		};
		SecurityContextHolder.getContext().setAuthentication(auth);
		
		CaNhan c1 = new CaNhan();
		c1.setName("test1");
		c1.setNgaySinh(new Date());
		c1.setGioiTinh("Nam");
		c1.setSoCMND("12345678901");
		c1.setDiaChi("Ha Noi");
		c1.setEmail("test@gmail.com");
		c1.setSoDT("0123456789");
		c1.setThanhPho("Ha Noi");
		c1.setquan("Ha Noi");
		caNhanController.addCaNhan(c1);
		
		HoGiaDinh hoGiaDinh=new HoGiaDinh();
    	hoGiaDinhRepository.save(hoGiaDinh);
    	c1.setHoGiaDinh(hoGiaDinh);
    	caNhanRepository.save(c1);
		
		CaNhan c2 = new CaNhan();
		c2.setName("test2");
		c2.setNgaySinh(new Date());
		c2.setGioiTinh("Nam");
		c2.setSoCMND("4521578945");
		c2.setDiaChi("Ha Noi");
		c2.setEmail("test2@gmail.com");
		c2.setSoDT("0123456789");
		c2.setThanhPho("Ha Noi");
		c2.setquan("Ha Noi");
		hoGiaDinhController.addThanhvien(c2);
		
		Model model = new Model() {

			@Override
			public Model mergeAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean containsAttribute(String attributeName) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Map<String, Object> asMap() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Model addAttribute(String attributeName, Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Model addAttribute(Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Model addAllAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Model addAllAttributes(Collection<?> attributeValues) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		String s = hoGiaDinhController.dsDangKi(model);
		User u = userService.getUser("test");
		List<CaNhan> list=caNhanRepository.findAllHGDbyUserId(u.getId());
		assertEquals("mua_bhyt", s);
		assertEquals(2, list.size());
	}
	
	@Test
	void testAddThanhvien_HoGiaDinhController() {
		BCryptPasswordEncoder passwordEncoder = this.context.getBean(BCryptPasswordEncoder.class);
		CaNhanRepository caNhanRepository = this.context.getBean(CaNhanRepository.class);
		HoGiaDinhRepository hoGiaDinhRepository = this.context.getBean(HoGiaDinhRepository.class);
		UserService userService = this.context.getBean(UserService.class);
		CaNhanController caNhanController = this.context.getBean(CaNhanController.class);
		
		UserRegistrationDto user = new UserRegistrationDto();
		user.setFirstName("Hieu");
		user.setLastName("Nguyen");
		user.setEmail("test");
		user.setPassword("123456");
		userService.save(user);
		
		Authentication auth = new Authentication() {

			public Object getPrincipal() {
				UserDetails user = userService.loadUserByUsername("test");
				return user;
			}

			@Override
			public Object getDetails() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getCredentials() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAuthenticated() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
				// TODO Auto-generated method stub

			}
		};
		SecurityContextHolder.getContext().setAuthentication(auth);
		
		CaNhan c1 = new CaNhan();
		c1.setName("test1");
		c1.setNgaySinh(new Date());
		c1.setGioiTinh("Nam");
		c1.setSoCMND("12345678901");
		c1.setDiaChi("Ha Noi");
		c1.setEmail("test@gmail.com");
		c1.setSoDT("0123456789");
		c1.setThanhPho("Ha Noi");
		c1.setquan("Ha Noi");
		caNhanController.addCaNhan(c1);
		
		HoGiaDinh hoGiaDinh=new HoGiaDinh();
    	hoGiaDinhRepository.save(hoGiaDinh);
    	c1.setHoGiaDinh(hoGiaDinh);
    	caNhanRepository.save(c1);
		
		CaNhan c2 = new CaNhan();
		c2.setName("test2");
		c2.setNgaySinh(new Date());
		c2.setGioiTinh("Nam");
		c2.setSoCMND("4521578945");
		c2.setDiaChi("Ha Noi");
		c2.setEmail("test2@gmail.com");
		c2.setSoDT("0123456789");
		c2.setThanhPho("Ha Noi");
		c2.setquan("Ha Noi");
		
		String s = hoGiaDinhController.addThanhvien(c2);
		User u = userService.getUser("test");
		List<CaNhan> list=caNhanRepository.findAllHGDbyUserId(u.getId());
		assertEquals("redirect:/hogiadinh/list?success", s);
		assertEquals(2, list.size());
	}

	@Test
	void testDeleteHoGiaDinh_HoGiaDinhController_ChiCo1ThanhVien() {
		BCryptPasswordEncoder passwordEncoder = this.context.getBean(BCryptPasswordEncoder.class);
		CaNhanRepository caNhanRepository = this.context.getBean(CaNhanRepository.class);
		HoGiaDinhRepository hoGiaDinhRepository = this.context.getBean(HoGiaDinhRepository.class);
		UserService userService = this.context.getBean(UserService.class);
		CaNhanController caNhanController = this.context.getBean(CaNhanController.class);
		
		UserRegistrationDto user = new UserRegistrationDto();
		user.setFirstName("Hieu");
		user.setLastName("Nguyen");
		user.setEmail("test");
		user.setPassword("123456");
		userService.save(user);
		
		Authentication auth = new Authentication() {

			public Object getPrincipal() {
				UserDetails user = userService.loadUserByUsername("test");
				return user;
			}

			@Override
			public Object getDetails() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getCredentials() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAuthenticated() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
				// TODO Auto-generated method stub

			}
		};
		SecurityContextHolder.getContext().setAuthentication(auth);
		
		CaNhan c1 = new CaNhan();
		c1.setName("test1");
		c1.setNgaySinh(new Date());
		c1.setGioiTinh("Nam");
		c1.setSoCMND("12345678901");
		c1.setDiaChi("Ha Noi");
		c1.setEmail("test@gmail.com");
		c1.setSoDT("0123456789");
		c1.setThanhPho("Ha Noi");
		c1.setquan("Ha Noi");
		caNhanController.addCaNhan(c1);
		
		HoGiaDinh hoGiaDinh=new HoGiaDinh();
    	hoGiaDinhRepository.save(hoGiaDinh);
    	c1.setHoGiaDinh(hoGiaDinh);
    	caNhanRepository.save(c1);
		Model model = new Model() {
			
			@Override
			public Model mergeAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean containsAttribute(String attributeName) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public Map<String, Object> asMap() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(String attributeName, Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Collection<?> attributeValues) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		User u = userService.getUser("test");
		CaNhan caNhan = caNhanRepository.findbyUserId(u.getId());
		String s = hoGiaDinhController.deleteHoGiaDinh(caNhan.getId(), model);
		assertEquals("redirect:/hogiadinh/list?error", s);
	}
	
	@Test
	void testDeleteHoGiaDinh_HoGiaDinhController_ChiCoNhieuHon1ThanhVien() {
		BCryptPasswordEncoder passwordEncoder = this.context.getBean(BCryptPasswordEncoder.class);
		CaNhanRepository caNhanRepository = this.context.getBean(CaNhanRepository.class);
		HoGiaDinhRepository hoGiaDinhRepository = this.context.getBean(HoGiaDinhRepository.class);
		UserService userService = this.context.getBean(UserService.class);
		CaNhanController caNhanController = this.context.getBean(CaNhanController.class);
		
		UserRegistrationDto user = new UserRegistrationDto();
		user.setFirstName("Hieu");
		user.setLastName("Nguyen");
		user.setEmail("test");
		user.setPassword("123456");
		userService.save(user);
		
		Authentication auth = new Authentication() {

			public Object getPrincipal() {
				UserDetails user = userService.loadUserByUsername("test");
				return user;
			}

			@Override
			public Object getDetails() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getCredentials() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAuthenticated() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
				// TODO Auto-generated method stub

			}
		};
		SecurityContextHolder.getContext().setAuthentication(auth);
		
		CaNhan c1 = new CaNhan();
		c1.setName("test1");
		c1.setNgaySinh(new Date());
		c1.setGioiTinh("Nam");
		c1.setSoCMND("12345678901");
		c1.setDiaChi("Ha Noi");
		c1.setEmail("test@gmail.com");
		c1.setSoDT("0123456789");
		c1.setThanhPho("Ha Noi");
		c1.setquan("Ha Noi");
		caNhanController.addCaNhan(c1);
		
		HoGiaDinh hoGiaDinh=new HoGiaDinh();
    	hoGiaDinhRepository.save(hoGiaDinh);
    	c1.setHoGiaDinh(hoGiaDinh);
    	caNhanRepository.save(c1);
		
		CaNhan c2 = new CaNhan();
		c2.setName("test2");
		c2.setNgaySinh(new Date());
		c2.setGioiTinh("Nam");
		c2.setSoCMND("4521578945");
		c2.setDiaChi("Ha Noi");
		c2.setEmail("test2@gmail.com");
		c2.setSoDT("0123456789");
		c2.setThanhPho("Ha Noi");
		c2.setquan("Ha Noi");
		hoGiaDinhController.addThanhvien(c2);
		
		Model model = new Model() {
			
			@Override
			public Model mergeAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean containsAttribute(String attributeName) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public Map<String, Object> asMap() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(String attributeName, Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Collection<?> attributeValues) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		User u = userService.getUser("test");
		List<CaNhan> list=caNhanRepository.findAllHGDbyUserId(u.getId());
		CaNhan cv = list.get(list.size() - 1);
		String s = hoGiaDinhController.deleteHoGiaDinh(cv.getId(), model);
		List<CaNhan> lc =caNhanRepository.findAllHGDbyUserId(u.getId());
		assertEquals("redirect:/hogiadinh/list?delete", s);
		assertEquals(1, lc.size());
	}
}
