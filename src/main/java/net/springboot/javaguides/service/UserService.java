package net.springboot.javaguides.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import net.springboot.javaguides.controller.dto.UserRegistrationDto;
import net.springboot.javaguides.entity.User;

@Configuration
@ComponentScan("net.spring.javaguides.service")
public interface UserService extends UserDetailsService{
	User save(UserRegistrationDto registrationDto);
	User getUser(String username);
}
