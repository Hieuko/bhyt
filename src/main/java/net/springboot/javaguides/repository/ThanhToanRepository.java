package net.springboot.javaguides.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import net.springboot.javaguides.entity.ThanhToan;

@Repository
public interface ThanhToanRepository extends JpaRepository<ThanhToan, Long>{
	
	@Query(value = "select thanhtoan.id,thanhtoan.magiaodich,thanhtoan.noidung,thanhtoan.phuongthuc,thanhtoan.sohoadon,thanhtoan.sotien,thanhtoan.thoigian "
			+ "from thanhtoan, (select thanhtoan_id from user_payment where user_id=?1) AS A where thanhtoan.id=A.thanhtoan_id",nativeQuery = true)
	List<ThanhToan> findbyUserId(Long id);
}
