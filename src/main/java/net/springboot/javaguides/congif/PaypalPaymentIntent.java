package net.springboot.javaguides.congif;

public enum PaypalPaymentIntent {
	sale, authorize, order
}
