package net.springboot.javaguides.congif;

public enum PaypalPaymentMethod {
	credit_card, paypal
}
