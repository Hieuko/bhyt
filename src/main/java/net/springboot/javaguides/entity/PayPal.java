package net.springboot.javaguides.entity;

public class PayPal {
	private int price;
	private String description;
	public PayPal() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PayPal(int price, String description) {
		super();
		this.price = price;
		this.description = description;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
