package net.springboot.javaguides.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import net.springboot.javaguides.entity.DoanhNghiep;
import net.springboot.javaguides.entity.NhanVien;
import net.springboot.javaguides.entity.User;
import net.springboot.javaguides.repository.DoanhNghiepRepository;
import net.springboot.javaguides.repository.NhanVienRepository;
import net.springboot.javaguides.service.UserService;
import net.springboot.javaguides.service.UserServiceImpl;

@Controller
@RequestMapping("nhanvien")
public class NhanVienController {
	@Autowired
	private NhanVienRepository nhanVienRepository;
	@Autowired
	private DoanhNghiepRepository doanhNghiepRepository;
	@Autowired
	private UserService userService;
	
	@GetMapping
	public String showForm(NhanVien nhanvien) {
		return "add-nhan-vien";
	}
	@PostMapping
	public String newNhanVien(NhanVien nhanVien) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetail = (UserDetails) auth.getPrincipal();
        User u = userService.getUser(userDetail.getUsername());
        
        Long user_id=u.getId();
        DoanhNghiep doanhNghiep=doanhNghiepRepository.findbyUserId(user_id);
        nhanVien.setDoanhNghiep(doanhNghiep);
        nhanVienRepository.save(nhanVien);
		return "redirect:/doanhnghiep";
	}
	
	@GetMapping("/delete/{id}")
	public String deleteNhanVien(@PathVariable("id")Long id) {
		nhanVienRepository.delete(nhanVienRepository.findById(id).orElseThrow());
		return "redirect:/doanhnghiep";
	}
}
